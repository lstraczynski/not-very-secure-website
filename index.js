import express from 'express';
import cookieParser from 'cookie-parser';
import morgan from 'morgan';
import router from './router.js';

const app = express();

app.set('view engine', 'ejs');
app.use(express.json());
app.use(cookieParser('abc'));
app.use(express.urlencoded({ extended: true }));
app.use(morgan('combined'));
app.use(router);

app.listen(50000);
console.log('Listening on http://localhost:50000')